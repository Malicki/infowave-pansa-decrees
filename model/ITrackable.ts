export interface ITrackable {
  Id: number | null,
  Created: Date | null,
  Modified: Date | null,
  AuthorId: number | null,
  ModifiedById: number | null,
  Version: String | null
}
