export interface IFile {
  Id?: string,
  Name?: string,
  OriginName: string,
  Type: string,
  Extension: string,
  Size: number,
  Store?: string,
  Content?: ArrayBuffer,
  Public: boolean,
  Order?: number
}
