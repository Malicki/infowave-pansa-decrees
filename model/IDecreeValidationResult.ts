import { IDecree } from './IDecree'

export interface IDecreeValidationError {
  Prop: string
  Message: string
}

export interface IDecreeValidationResult {
  IsValid: boolean
  Message: string
  Errors?: Array<IDecreeValidationError>
  Source: string
}

export interface IValidateDecree {
  (arg: IDecree): Promise<IDecreeValidationResult>
}

export interface IValidateField {
  <T>(prop: string, arg: T): IDecreeValidationError
}
