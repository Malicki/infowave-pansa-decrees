export interface IGetDecrees {
  Count?: Boolean,
  Id: number | null,
  Skip: number,
  Top: number,
  OrderBy: string | null,
  Desc: Boolean,
  ExpandDictionaries?: Boolean,
  Search?: string,
  Filter?: IGetDecreesFilter | null,
  Deleted?: boolean
}

export interface IGetDecreesFilter {
  Uid: number[] | null,
  Type: Array<number>,
  Publisher: Array<number>,
  Status: Array<number>,
  Case: string | null,
  Content: string | null,
  Teams: boolean | null,
  Projects: boolean | null,
  PublicProcurement: boolean | null,
  Tags: Array<number>
  Date: Array<Date>
}
