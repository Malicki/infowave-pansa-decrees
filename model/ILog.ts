export interface ILog {
  Date: Date,
  Event: string,
  EventId: string,
  UserName: string,
  UserId: string,
  Bullets?: string[]
}
