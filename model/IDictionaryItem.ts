export interface IDictionaryItem {
  Id: number,
  Name: string,
  Archived: boolean
}

export interface IDecreeType extends IDictionaryItem {
  Preview: string,
  FileName: string,
  Genitive: string
}
export interface IPublisher extends IDictionaryItem {
  Preview: string,
  FileName: string
}
export interface IDecreeTag extends IDictionaryItem {}
export interface IDecreeStatus extends IDictionaryItem {
  InternalName: string
}

export interface IDictBag {
  Version: String,
  DecreeTypes: Array<IDecreeType>,
  Publishers: Array<IPublisher>,
  DecreeTags: Array<IDecreeTag>,
  DecreeStatuses: Array<IDecreeStatus>
}
