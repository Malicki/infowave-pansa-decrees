export interface IScenarioStep {
  Action: string
  Text?: string
  Point?: string
  Constraint?: IScenarioStepConstraint
  Dock?: number,
  Center?: boolean
}

export interface IScenario {
  Name: string
  Description?: string
  Steps: IScenarioStep[]
}

export interface IScenarioStepConstraint {
  RouteName?: string
  RouteVariant?: string
}

export interface IScenarioPlayer {
  PlayScenario (scenario: IScenario)
}
