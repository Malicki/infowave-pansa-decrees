import { ITrackable } from 'model/ITrackable'
import { IDecreeType, IPublisher, IDecreeTag, IDecreeStatus } from './IDictionaryItem'
import { ILog } from './ILog'
import { IFile } from 'model/IFile'

export interface IDecree extends ITrackable {

  TypeId: number | null,
  PublisherId: number | null,
  StatusId: number | null,
  Date: Date | null,
  /*built-in tags*/
  Projects: Boolean,
  Teams: Boolean,
  PublicProcurement: Boolean,

  Uid: number | null,
  TagIds: Array<number>,

  Case: string,
  Content: string,

  Deleted?: boolean,
  DeletedById?: number,
  DeletedDate?: Date,

  // CanceledId?: number
  // ChangedId?: number
  CanceledById?: number

  CanceledIds: number[],
  ChangedIds: number[],

  // navigation
  Type?: IDecreeType,
  Publisher?: IPublisher,
  Tags?: Array<IDecreeTag>,
  Status?: IDecreeStatus
  Logs?: Array<ILog>
  Master?: IFile | null
  Canceled?: Array<IDecree>
  Changed?: Array<IDecree>
  CanceledBy?: IDecree
  Attachments?: Array<IFile>
  ChangedBy?: Array<IDecree>
  ChangedByAny?: boolean
}

export interface IDecreeRelationships {
  Subject: IDecree,

  Canceled?: Array<IDecree>,
  PendingCanceled?: Array<IDecree>,

  CanceledBy?: IDecree,
  PendingCanceledBy?: Array<IDecree>,

  Changed?: Array<IDecree>,
  PendingChanged?: Array<IDecree>,

  ChangedBy?: Array<IDecree>,
  PendingChangedBy?: Array<IDecree>,
}
