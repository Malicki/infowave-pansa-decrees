export interface IResult<T> {
  Value: Array<T>,
  Count: number | null
}
