// import NuxtConfiguration from '@nuxt/config-edge'

const config /* : NuxtConfiguration */ = {
  head: {
    title: 'PANSA - Akty zarządcze',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'pansa Akty zarządcze by Mateusz Malicki' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/pansa.ico' }, { rel: 'stylesheet', href: '/main.css' }]
  },
  loading: { color: 'hsl(240, 33%, 70%)' },
  // css: ['element-ui/lib/theme-chalk/index.css'],
  css: ['@/assets/element-pansa.scss'],
  // plugins: [],
  modules: [
    'nuxt-element-ui', '~/modules/meta'
  ],
  plugins: ['~/plugins/componentHooks', '~/plugins/testDataVersion', '~/plugins/keys', {
    src: '~/plugins/auth', ssr: false
  }, {
    src: '~/plugins/panicDump', ssr: false
  }],
  elementUI: {
    components: [
      'Progress',
      'Upload',
      'Collapse',
      'CollapseItem',
      'MessageBox',
      'Card',
      'Timeline',
      'TimelineItem',
      'Breadcrumb',
      'BreadcrumbItem',
      'ButtonGroup',
      'Container',
      'Main',
      'Footer',
      'Aside',
      'Header',
      'Badge',
      'Pagination',
      'Notification',
      'Button',
      'Input',
      'DatePicker',
      'Dropdown',
      'DropdownMenu',
      'Menu',
      'Submenu',
      'MenuItem',
      'DropdownItem',
      'Row',
      'Col',
      'Select',
      'Switch',
      'Option',
      'Tag',
      'Checkbox',
      'Table',
      'TableColumn',
      'Loading',
      'Scrollbar',
      'Tooltip',
      'Popover',
      'InputNumber',
      'Slider',
      'Step',
      'Steps',
      'Tabs',
      'TabPane',
      'Dialog'],
    locale: 'pl'
  },
  router: {
    middleware: ['auth'],
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'not_found',
        path: '*',
        component: resolve(__dirname, 'pages/404.vue')
      })
    }
  },
  build: {
    extractCSS: true,
    cache: false,      // ⚠️ Experimental
    parallel: false    // ⚠️ Experimental
  }
}

export default config
