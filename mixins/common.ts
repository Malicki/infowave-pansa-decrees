import Vue from 'vue'
import Component from 'vue-class-component'

const months = ['Stycznia', 'Lutego', 'Marca', 'Kwietnia', 'Maja', 'Czerwca', 'Lipca', 'Sierpnia', 'Września', 'Października', 'Listopada', 'Grudnia']
const loaders: any[] = []
let service: any = null
let locked: boolean = false

function getRandomInt (min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

const successTitles = ['Brawo', 'Sukces', 'Udało się', 'Świetnie']
const errorTitles = ['O nie', ':-(']
const offset = 50
const duration = 3000

@Component
export default class ClientHooks extends Vue {
  created () {
    if (!process.server && typeof this['$Created'] === 'function') {
      this['$Created']()
    }
    if (!process.server && typeof this['$OnResize'] === 'function') {
      this['$OnResize']()
      window.addEventListener('resize', this['$OnResize'], { passive: true })
    }
    if (!process.server && typeof this['$OnBeforeunload'] === 'function') {
      window.addEventListener('beforeunload', this['$OnBeforeunload'], false)
    }
    if (!process.server && (typeof this['$Dump'] === 'function')) {
      window['--dump-current--'] = () => {
        return {
          key: '--dump--' + this.$route.path.replace('/','').replace('/','').replace('/','').replace('/',''),
          value: JSON.stringify(this['$Dump'](true) || null)
        }
      }
    }
  }
  mounted () {
    if (!process.server && typeof this['$Mounted'] === 'function') {
      this['$Mounted']()
    }
    if (!process.server && this.$route.params.$success) {
      const msg = this.$route.params.$success
      this.$route.params.$success = ''
      this.$Success(msg)
    }
  }
  beforeDestroy () {
    if (!process.server) {
      if (typeof this['$OnResize'] === 'function') {
        window.removeEventListener('resize', this['$OnResize'], false)
      }
      if (typeof this['$OnBeforeunload'] === 'function') {
        window.removeEventListener('beforeunload', this['$OnBeforeunload'], false)
      }
      this.$Loaded() // just to be sure ;)
    }
  }
  GetRandomInt (min: number, max: number): number {
    return getRandomInt(min, max)
  }
  beforeRouteLeave (_, __, next) {
    console.log('beforeRouteLeave mixin')
    window['--dump-current--'] = null
    if (typeof this['$Dump'] === 'function') {
      const res = this['$Dump'](false)
      const key = '--dump--' + this.$route.path.replace('/','').replace('/','').replace('/','').replace('/','')
      window.sessionStorage.setItem(key, JSON.stringify(res))
    }
    next()
  }

  $Restore () {
    if (this.$route.matched.length > 0 && this.$route.matched[0].instances.default === this) {
      if (typeof this['$Dump'] === 'function') {
        const key = '--dump--' + this.$route.path.replace('/','').replace('/','').replace('/','').replace('/','')
        const res = JSON.parse(window.sessionStorage.getItem(key) || 'null')
        window.sessionStorage.removeItem(key)
        return res
      }
      return null
    }
    return null
  }

  $Loading (message: string = '') {
    if (!process.server) {
      if (loaders.indexOf(this) < 0) {
        loaders.push(this)
        // console.log('loading: ' + this['_uid'])
      }

      this.$nextTick(() => {
        const target = document.querySelector('.el-main')
        // const mainH = target!.getBoundingClientRect().height
        if (locked === false && ((!service) || (!service.visible))) {
          locked = true
          // console.log('loading invoked: ' + this['_uid'])
          const s = this['$loading']({
            spinner: 'el-icon-loading-spinner',
            target: target,
            text: 'podchodzimy do ładowania...'
          })
          if (s !== service && service) {
            // console.log('got new service: ' + this['_uid'])
            window['oldservice'] = service
          }
          service = s
          // service.$el.firstChild.style.top = 'initial'
          // service.$el.firstChild.style.bottom = '' + (mainH - window.scrollY - (window.screen.availHeight / 2) + 75) + 'px'

          service.$el.firstChild.style.top = 'initial'
          service.$el.firstChild.style.bottom = '' + (window.innerHeight / 2 - 70) + 'px'
          service.$el.firstChild.style.position = 'fixed'
          service.$el.firstChild.style.marginLeft = '-75px'
          if (message) {
            service.$el.querySelector('.el-loading-text').textContent = message
          }
          this.$nextTick(() => {
            locked = false
          })
        } else if (service) {
          // service.$el.firstChild.style.top = 'initial'
          // service.$el.firstChild.style.bottom = '' + (mainH - window.scrollY - (window.screen.availHeight / 2) + 75) + 'px'

          service.$el.firstChild.style.top = 'initial'
          service.$el.firstChild.style.bottom = '' + (window.innerHeight / 2 - 70) + 'px'
          service.$el.firstChild.style.position = 'fixed'
          service.$el.firstChild.style.marginLeft = '-75px'
          if (message) {
            service.$el.querySelector('.el-loading-text').textContent = message
          }
        }
      })
    }
  }
  $IsLoading (): boolean {
    return loaders.length > 0
  }
  $Loaded () {
    if (!process.server) {
      const index = loaders.findIndex(_ => _ === this)
      if (index >= 0) {
        loaders.splice(index, 1)
        // console.log('loaded: ' + this['_uid'])
      }
      if (loaders.length === 0 && service) {
        setTimeout(() => {
          if (loaders.length === 0 && service) {
            service.close()
          }
        }, 100)
      } else {
        // console.log('loaded ignored: ' + this['_uid'] + ' -> because: ' + (loaders.length !== 0 ? ('loaders not empty(' + loaders.length + ')') : 'no service'))
      }
    }
  }
  $Success (message: string, title: string = ''): void {
    this['$notify']({ title : title || successTitles[getRandomInt(0, successTitles.length - 1)], message, type: 'success', offset: offset, duration: duration })
  }
  $Error (message: string, title: string = ''): void {
    this['$notify']({ title : title || errorTitles[getRandomInt(0, errorTitles.length - 1)], message, type: 'error', offset: offset, duration: duration })
  }
  $Alert (message: string, title: string = 'info', type: string = 'info', html: boolean = false): void {
    return this['$alert'](message, title || 'info', {
      confirmButtonText: 'OK',
      type: type,
      dangerouslyUseHTMLString: html
    })
  }
  $Confirm (message: string, title: string, conf: any): Promise<void> {
    return this['$confirm'](message, title, conf || {})
  }
  async $SafeConfirm (confirm: any): Promise<boolean> {
    try {
      await this.$Confirm(confirm.message, confirm.title || 'Potwierdź', {
        confirmButtonText: confirm.ok || 'Tak',
        cancelButtonText: confirm.cancel || 'Anuluj',
        type: 'warning'
      })
      return true
    } catch {
      return false
    }
  }
  $CloneObject (obj: any): any {
    return JSON.parse(JSON.stringify(obj))
  }
  $ZeroPad (n: number, length: number = 2): string {
    let str = n.toString() + ''
    while (str.length < length) str = '0' + str
    return str
  }
  $FormatDate (date: Date | null): string {
    if (!date) {
      return '-'
    }
    return `${date.getFullYear()}-${ this.$ZeroPad(date.getMonth() + 1) }-${ this.$ZeroPad(date.getDate()) }`
  }
  $FormatDateTime (date: Date | null): string {
    if (!date) {
      return '-'
    }
    return `${ this.$FormatDate(date) } ${ this.$ZeroPad(date.getHours()) }:${ this.$ZeroPad(date.getMinutes()) }:${ this.$ZeroPad(date.getSeconds()) }`
  }
  $FormatDecreeDate (date: Date): string {
    return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()} roku`
  }
}
