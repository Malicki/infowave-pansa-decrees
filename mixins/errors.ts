import Vue from 'vue'
import Component from 'vue-class-component'

const numericErrors = [400, 401, 403, 404, 412, 422, 500, 503]

// @ts-ignore
function default_redirect (vm, key, err) {
  const options = {
    mode: 'push',
    to: '/500'
  }
  if (err.statusCode === 404 && vm.$route.name.indexOf('decrees-') === 0) {
    options.to = '/decrees/404'
  } else if (numericErrors.indexOf(err.statusCode) >= 0) {
    options.to = '/' + err.statusCode
  }
  return options
}
// @ts-ignore
function default_alert (vm, key, err) {
  const options = {
    title: 'O nie!',
    message: 'Coś poszło nie tak...\n"$message"',
    type: 'error'
  }
  if (key === '$412') {
    options.title = 'Niepasujący numer wersji'
    options.message = 'Ktoś inny edytował ten element.\n Odśwież stronę, aby móc ponowić akcję.'
  } else if (key === '$400') {
    options.title = '400 - walidacja'
    options.message = err.message || 'Coś poszło nie tak...'
  } else if (key === '$422') {
    options.title = '422 - walidacja'
    options.message = err.message || 'Coś poszło nie tak...'
  } else if (key === '$401') {
    options.title = '401 - Uwierzytelnianie'
    options.message = 'Wygląda na to, że twoje poświadczenia wygasły.\nOdśwież stronę, aby zalogować się ponownie.'
  } else if (key === '$503') {
    options.title = '503 - Usługa niedostępna'
    options.message = 'Wygląda na to, że mamy przerwę techniczną.\nSpróbuj ponownie za kilka minut.'
  }
  return options
}

const actions = {
  redirect (vm, key, err, options) {
    options = Object.assign(default_redirect(vm, key, err), options || {})
    vm.$router[options.mode](options.to)
  },
  alert (vm, key, err, options) {
    options = Object.assign(default_alert(vm, key, err), options || {})
    options.message = options.message.replace('$message', err.message || '')
    vm.$Alert(options.message, options.title, options.type)
  },
  // @ts-ignore
  invoke (vm, key, err, options) {
    options()
  },
  pass (vm, key, err, options) {
    options(vm, key, err)
  },
  // @ts-ignore
  $throw (vm, key, err, options) {
    throw err
  }
}

const defaultActions = {
  $400: 'alert',
  $401: 'alert',
  $403: 'redirect',
  $404: 'redirect',
  $412: 'alert',
  $422: 'alert',
  $500: 'redirect',
  $503: 'alert',
  'unknown': 'redirect'
}

function getErrorKey (err: any) {
  if (numericErrors.indexOf(err.statusCode) >= 0) {
    return '$' + err.statusCode
  } else {
    return 'unknown'
  }
}

@Component
export default class Errors extends Vue {
  $HandleError (err: Error, overrides: any = null) {
    overrides = overrides || {}
    const key = getErrorKey(err)
    let override = overrides[key]
    if (typeof override === 'function') {
      override = override(this, key, err)
    }
    const actionKey = override && override.action ? override.action : defaultActions[key]
    const options = override && override.options ? override.options : {}
    const action = actions[actionKey] ? actions[actionKey] : actions['$throw']
    action(this, key, err, options)
  }
}
