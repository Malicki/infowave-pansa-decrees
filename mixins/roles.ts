import Vue from 'vue'
import Component from 'vue-class-component'
import * as user from '~/store/modules/User'
import { State } from 'vuex-class'
// const User = namespace(user.name)
const ifServer = true
@Component
export default class Roles extends Vue {
  @State('User') $$$User!: user.State
  $IsInRole (role: string): boolean {
    if (process.server) {
      return ifServer
    }
    return this.$$$User.CurrentUser!.Roles.indexOf(role.toLowerCase()) >= 0
  }
  get $IsAdmin (): boolean {
    return this.$IsInRole('admin')
  }
  get $IsSekretariat (): boolean {
    return this.$IsInRole('sekretariat')
  }
}
