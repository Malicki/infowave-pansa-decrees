var _source = null;
var _origin = null;
var _ts = false;

var ctx = {};

var AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;



async function run (data) {
  try {
    var fn = new AsyncFunction('ctx', data)
  }  catch {
    _source.postMessage({
      command: _ts ? 'invalid-ts' : 'invalid',
      data: data
    }, _origin)
    return
  }
  _source.postMessage({
    command: _ts ? 'correct-ts' : 'correct',
    data: data
  }, _origin)
  try {
    await fn(ctx)
  } catch (err) {
    _source.postMessage({
      command: 'error',
      data: err.message
    }, _origin)
  }
}

function printError (err) {
  _source.postMessage({
    command: 'error',
    data: err.message
  }, _origin)
}

function receiveMessage(event) {
  _source = event.source
  _origin = event.origin


  var x = JSON.parse(event.data)
  if (x.command === 'run') {
    _ts = false
    run(x.data)
  } else if (x.command === 'run-ts') {
    _ts = true
    run(x.data)
  } else if (x.command === 'rpc-def') {
    var rpc = x.data
    print('available remote procedures:\n - ' + rpc.join('\n - '))
    rpc.forEach( (_) => {
      window[_] = function (arg) {
        var uid = '' + (new Date().getTime()) + Math.random();
        _source.postMessage({
          command: 'rpc',
          data: {
            proc: _,
            uid: uid,
            arg: arg
          }
        }, _origin);
        return new Promise(function(resolve, reject) {
          var handle = function(ev) {
            var d = typeof ev.data === 'string' ? JSON.parse(ev.data) : ev.data
            if (d.command === 'rpc-response' && d.data.uid === uid) {
              ev.preventDefault()
              window.removeEventListener('message', handle, false)
              resolve(d.data.result)
            } else if (d.command === 'rpc-error' && d.data.uid === uid) {
              ev.preventDefault()
              window.removeEventListener('message', handle, false)
              // printError(d.data.error)
              reject(d.data.error)
            }
          }
          window.addEventListener('message', handle, false)
        })
      }
    })
  }

  
  
}
function printTable (data) {
  _source.postMessage({
    command: 'printTable',
    data: data
  }, _origin)
}
function print (data) {
  if (typeof data === 'undefined') {
    data = 'undefined'
  }
  if (data === null) {
    data = 'null'
  }
  if (typeof data !== 'string') {
    if (typeof data === 'object') {
      data = JSON.stringify(data, null, 2)
    }
    else {
      data = '' + data
    }
  }
  _source.postMessage({
    command: 'printLn',
    data: data
  }, _origin)
}

window.addEventListener('message', receiveMessage, false)