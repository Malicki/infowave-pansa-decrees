import { $root } from '~/store/root'

const anonymous = ['index', 'signin', 'adfs', '404', 'not_found', '403', '500']
const sekretariat = ['decrees-create', 'decrees-edit', 'decrees-files', 'generator', 'dictionaries']
const admin = ['healthcheck']

export default function ({ store, route, redirect }) {
  if (!process.server) {
    if (!$root(store.state).User.Authorized) {
      if (anonymous.indexOf(route.name) < 0) {
        redirect('/signin?next=' + encodeURIComponent(route.fullPath))
      }
    } else {
      const roles = $root(store.state).User.CurrentUser!.Roles
      if (admin.indexOf(route.name) >= 0) {
        if (roles.indexOf('admin') < 0) {
          redirect('/403')
        }
      } else if (sekretariat.indexOf(route.name) >= 0) {
        if (roles.indexOf('admin') < 0 && roles.indexOf('sekretariat') < 0) {
          redirect('/403')
        }
      }
    }
  }
}
