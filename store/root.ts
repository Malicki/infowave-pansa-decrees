import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from 'store'
import { State as UserState } from './modules/User'
import { State as DecreesState } from './modules/Decrees'
import { State as DictionariesState } from './modules/Dictionaries'

export const types = {}

export interface State {}

export const state = (): State => ({})

export const getters: GetterTree<State, RootState> = {}

export interface Actions<S, R> extends ActionTree<S, R> {}

export const actions: Actions<State, RootState> = {}

export const mutations: MutationTree<State> = {}

export interface RootStateWithModules {
  User: UserState,
  Dictionaries: DictionariesState,
  Decrees: DecreesState
}

export function $root (rootState: State): RootStateWithModules {
  return rootState as RootStateWithModules
}
