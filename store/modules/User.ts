import { RootState } from 'store'
import { GetterTree, MutationTree, ActionTree, ActionContext } from 'vuex'

export const name = 'User'
export const namespaced = true

const usersByCode = {
  '607a5637-07ed-4721-b2f7-3cf69852834a': {
    Id: 12,
    Title: 'Krystyna Czubówna',
    Roles: ['admin', 'user']
  },
  'b9efa75c-a98d-4b9e-940b-bcdcdd14c42d': {
    Id: 666,
    Title: 'Jan Kowalski',
    Roles: ['admin']
  },
  '7be68a52-46ff-4438-9427-27a3c667bf00': {
    Id: 667,
    Title: 'Piotr Konieczny',
    Roles: ['sekretariat']
  },
  'a74c13b6-9140-4a4c-84e7-a8693091a5dc': {
    Id: 668,
    Title: 'Alicja Makota',
    Roles: ['user', 'sekretariat']
  },
  '4801d57e-c5e5-44ea-b4fd-7fc720f30a1c': {
    Id: 669,
    Title: 'Jan Bezziemi',
    Roles: ['user']
  },
  '61945d91-9ace-4b2b-a798-be830a68dcf5': {
    Id: 670,
    Title: 'Iwona Podbipięta',
    Roles: ['user', 'nieistotna']
  }
}

export interface State {
  CurrentUser?: IUser,
  Authorized: boolean
}

export interface IUser {
  Id: number,
  Title: string,
  Roles: string[]
}
export const state = (): State => {
  return { Authorized: false }
}

export const mutations: MutationTree<State> = {
  SignOut (state: State) {
    state.Authorized = false
    state.CurrentUser = undefined
    window.localStorage.removeItem('__lastCode')
  },
  SignIn (state: State, user: IUser) {
    state.Authorized = true
    state.CurrentUser = user
  }
}
export const getters: GetterTree<State, RootState> = {}

export interface Actions<S, R> extends ActionTree<S, R> {
  SignOut (context: ActionContext<S, R>): Promise<void>
  SignInWithCode (context: ActionContext<S, R>, code: string): Promise<IUser>
}

export const actions: Actions<State, RootState> = {
  async SignOut (context: ActionContext<State, RootState>): Promise<void> {
    context.commit('SignOut')
    return Promise.resolve()
  },
  async SignInWithCode (context: ActionContext<State, RootState>, code: string): Promise<IUser> {
    const user = usersByCode[code]
    if (user) {
      window.localStorage.setItem('__lastCode', code)
      context.commit('SignIn', user)
      return Promise.resolve(user)
    } else {
      const er = new Error('Nie udało się zalogować!')
      er['statusCode'] = 500
      throw er
    }
  }
}
