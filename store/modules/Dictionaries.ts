import { ActionTree, ActionContext } from 'vuex'
import { RootState } from 'store'
import { IDictBag, IDecreeType, IPublisher, IDecreeTag } from '~/model/IDictionaryItem'

export const name = 'Dictionaries'

const key = '__dictionaries'

const defaultBag: IDictBag = {
  Version: 'initial',
  DecreeTypes: [
    {
      Id: 1,
      Name: 'Zarządzenie',
      Preview: 'Zarządzenie',
      Genitive: 'Zarządzenie',
      FileName: 'ZARZADZENIE',
      Archived: false
    },
    {
      Id: 2,
      Name: 'Polecenie służbowe',
      Preview: 'Polecenie służbowe',
      Genitive: 'Polecenie służbowe',
      FileName: 'POLECENIE',
      Archived: false
    },
    {
      Id: 3,
      Name: 'Decyzja',
      Preview: 'Decyzja',
      Genitive: 'Decyzję',
      FileName: 'DECYZJA',
      Archived: false
    },
    {
      Id: 4,
      Name: 'Pismo okólne',
      Preview: 'Pismo okólne',
      Genitive: 'Pismo okólne',
      FileName: 'PISMO-OKOLNE',
      Archived: true
    },
    {
      Id: 5,
      Name: 'Komunikat',
      Preview: 'Komunikat',
      Genitive: 'Komunikat',
      FileName: 'KOMUNIKAT',
      Archived: true
    }
  ],
  Publishers: [
    {
      Id: 1,
      Name: 'Prezes',
      Preview: 'Prezesa',
      FileName: 'PREZES',
      Archived: false
    },
    {
      Id: 2,
      Name: 'Zastępca Prezesa ds. Żeglugi Powietrznej',
      Preview: 'Zastępcy Prezesa ds. Żeglugi Powietrznej',
      FileName: 'ZST-PRE-DS-ZEG-POW',
      Archived: false
    },
    {
      Id: 3,
      Name: 'Zastępca Prezesa ds. Finansowo-Administracyjnych',
      Preview: 'Zastępcy Prezesa ds. Finansowo-Administracyjnych',
      FileName: 'ZST-PRE-DS-FIN-ADM',
      Archived: false
    },
    {
      Id: 4,
      Name: 'Dyrektor Ośrodka Szkolenia Personelu ATS (AY)',
      Preview: 'Dyrektora Ośrodka Szkolenia Personelu ATS (AY)',
      FileName: 'DYR-OSR-SZKO-PER-ATS-AY',
      Archived: false
    },
    {
      Id: 5,
      Name: 'Dyrektor Biura Strategii i Współpracy Międzynarodowej (AX)',
      Preview: 'Dyrektora Biura Strategii i Współpracy Międzynarodowej (AX)',
      FileName: 'DYR-B-STRA-WSP-MIEDZ-AX',
      Archived: false
    },
    {
      Id: 10,
      Name: 'Dyrektor Biura Zarządzania Bezpieczeństwem Ruchu Lotniczego i Jakości Usług (AB)',
      Preview: 'Dyrektora Biura Zarządzania Bezpieczeństwem Ruchu Lotniczego i Jakości Usług (AB)',
      FileName: 'DYR-B-ZARZ-BEZP-RU-LOT-JAK-USL-AB',
      Archived: true
    },
    {
      Id: 11,
      Name: 'Dyrektor Biura Rozwoju i Wdrożeń (AW)',
      Preview: 'Dyrektora Biura Rozwoju i Wdrożeń (AW)',
      FileName: 'DYR-B-ROZ-WDRO-AW',
      Archived: true
    },
    {
      Id: 12,
      Name: 'Dyrektor Biura Szkolenia i Rozwoju Personelu ATM/CNS (AL)',
      Preview: 'Dyrektora Biura Szkolenia i Rozwoju Personelu ATM/CNS (AL)',
      FileName: 'DYR-B-SZKO-ROZW-PER-ATM-CNS-AL',
      Archived: true
    },
    {
      Id: 13,
      Name: 'Dyrektor Biura Zarządzania Bezpieczeństwem Ruchu Lotniczego i Jakości (AB)',
      Preview: 'Dyrektora Biura Zarządzania Bezpieczeństwem Ruchu Lotniczego i Jakości (AB)',
      FileName: 'DYR-B-ZARZ-BEZP-RU-LOT-JAK-AB',
      Archived: true
    },
    {
      Id: 14,
      Name: 'Dyrektor Biura Finansów (AF)',
      Preview: 'Dyrektora Biura Finansów (AF)',
      FileName: 'DYR-B-FIN-AF',
      Archived: true
    },
    {
      Id: 15,
      Name: 'Dyrektor Biura Rozwoju i Wdrożeń-Pełnomocnik ds. Realizacji Projektów Unijnych (AW)',
      Preview: 'Dyrektora Biura Rozwoju i Wdrożeń-Pełnomocnik ds. Realizacji Projektów Unijnych (AW)',
      FileName: 'DYR-B-ROZ-WDRO-PEL-DS-REA-PRO-UNI-AW',
      Archived: true
    },
    {
      Id: 16,
      Name: 'Dyrektor Biura Wdrożeń i Rozwoju-Pełnomocnik ds. Realizacji Projektów Unijnych (AW)',
      Preview: 'Dyrektora Biura Wdrożeń i Rozwoju-Pełnomocnik ds. Realizacji Projektów Unijnych (AW)',
      FileName: 'DYR-B-WDRO-ROZ-PEL-DS-REA-PRO-UNI-AW',
      Archived: true
    },
    {
      Id: 17,
      Name: 'Dyrektor Biura Zarządu i Organizacji (AQ)',
      Preview: 'Dyrektora Biura Zarządu i Organizacji (AQ)',
      FileName: 'DYR-B-ZARZ-ORG-AQ',
      Archived: true
    },
    {
      Id: 18,
      Name: 'Dyrektor Biura Rozwoju Infrastruktury (AW)',
      Preview: 'Dyrektora Biura Rozwoju Infrastruktury (AW)',
      FileName: 'DYR-B-ROZ-INFR-AW',
      Archived: true
    },
    {
      Id: 19,
      Name: 'Dyrektor Biura Bezpieczeństwa i Zarządzania Kryzysowego w Ruchu Lotniczym (AA)',
      Preview: 'Dyrektora Biura Bezpieczeństwa i Zarządzania Kryzysowego w Ruchu Lotniczym (AA)',
      FileName: 'DYR-B-BEZP-ZARZ-KRYZ-WU-LOT-AA',
      Archived: true
    },
    {
      Id: 20,
      Name: 'Dyrektor Biura Ekonomiczno-Finansowego (AF)',
      Preview: 'Dyrektora Biura Ekonomiczno-Finansowego (AF)',
      FileName: 'DYR-B-EKO-FIN-AF',
      Archived: true
    },
    {
      Id: 21,
      Name: 'Dyrektor Biura Wdrożeń i Rozwoju (AW)',
      Preview: 'Dyrektora Biura Wdrożeń i Rozwoju (AW)',
      FileName: 'DYR-B-WDRO-ROZ-AW',
      Archived: true
    }
  ],
  DecreeTags: [
    {
      Id: 1,
      Name: 'Regulamin',
      Archived: false
    },
    {
      Id: 2,
      Name: 'Instrukcja',
      Archived: false
    },
    {
      Id: 3,
      Name: 'Polityka',
      Archived: false
    }
  ],
  DecreeStatuses: [
    {
      Id: 1,
      Name: 'Robocze',
      InternalName: 'draft',
      Archived: false
    },
    {
      Id: 2,
      Name: 'Oczekujące na publikację',
      InternalName: 'uid_locked',
      Archived: false
    },
    {
      Id: 3,
      Name: 'Opublikowane',
      InternalName: 'published',
      Archived: false
    },
    {
      Id: 10,
      Name: 'Zaimportowany',
      InternalName: 'import',
      Archived: true
    }
  ]
}

async function fromLocalStorage (delay: number = 150): Promise<IDictBag> {
  if (window['__altDelay']) {
    delay = window['__altDelay'] as number
  }
  return new Promise(function (resolve) {
    const str = window.localStorage.getItem(key) || JSON.stringify(defaultBag)
    setTimeout(() => {
      resolve(JSON.parse(str) as IDictBag)
    }, delay)
  })
}

async function toLocalStorage (bag: IDictBag, delay: number = 150): Promise<void> {
  if (window['__altDelay']) {
    delay = window['__altDelay'] as number
  }
  return new Promise(function (resolve) {
    window.localStorage.setItem(key, JSON.stringify(bag))
    setTimeout(() => {
      resolve()
    }, delay)
  })
}

function sortByName (a, b) {
  const p = 'Name'
  const av = 1
  return a[p] >= b[p] ? (a[p] > b[p] ? av : 0) : (-av)
}

function sortDictCollection (col) {
  const archived = col.filter(_ => _.Archived)
  const _ = col.filter(_ => !_.Archived)
  return [..._.sort(sortByName), ...archived.sort(sortByName)]
}

export interface State {}

export const namespaced = true

export const state = (): State => ({})

export interface Actions<S, R> extends ActionTree<S, R> {
  GetDictionaries (context: ActionContext<S, R>): Promise<IDictBag>
  CreateDecreeType (context: ActionContext<S, R>, decreeType: IDecreeType): Promise<IDecreeType>
  UpdateDecreeType (context: ActionContext<S, R>, decreeType: IDecreeType): Promise<IDecreeType>
  CreatePublisher (context: ActionContext<S, R>, publisher: IPublisher): Promise<IPublisher>
  UpdatePublisher (context: ActionContext<S, R>, publisher: IPublisher): Promise<IPublisher>
  CreateDecreeTag (context: ActionContext<S, R>, decreeTag: IDecreeTag): Promise<IDecreeTag>
  UpdateDecreeTag (context: ActionContext<S, R>, decreeTag: IDecreeTag): Promise<IDecreeTag>
}
export const actions: Actions<State, RootState> = {
  async GetDictionaries (): Promise<IDictBag> {
    const bag = await fromLocalStorage()
    bag.Publishers = sortDictCollection(bag.Publishers)
    bag.DecreeTypes = sortDictCollection(bag.DecreeTypes)
    bag.DecreeTags = sortDictCollection(bag.DecreeTags)

    return bag
  },
  async CreateDecreeType ({}, decreeType: IDecreeType): Promise<IDecreeType> {
    if (!decreeType.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!decreeType.Preview) {
      const er = new Error('Nazwa wyświetlana jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!decreeType.FileName) {
      const er = new Error('Nazwa pliku jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (decreeType.FileName.length > 150) {
      const er = new Error('Nazwa pliku jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (decreeType.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (decreeType.Preview.length > 250) {
      const er = new Error('Nazwa wyświetlana jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.DecreeTypes.find(_ => _.Name === decreeType.Name)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const id = bag.DecreeTypes.reduce((oa, u) => Math.max(oa, u.Id), 0) + 1
    const now = new Date()
    const created: IDecreeType = {
      Id: id,
      Name: decreeType.Name,
      Preview: decreeType.Preview,
      FileName: decreeType.FileName,
      Genitive: decreeType.Genitive,
      Archived: decreeType.Archived || false
    }
    bag.DecreeTypes.push(created)
    bag.Version = 'v' + now.getTime().toString()
    await toLocalStorage(bag)
    return created
  },
  async CreatePublisher ({}, publisher: IPublisher): Promise<IPublisher> {
    if (!publisher.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!publisher.Preview) {
      const er = new Error('Nazwa wyświetlana jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!publisher.FileName) {
      const er = new Error('Nazwa pliku jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.FileName.length > 150) {
      const er = new Error('Nazwa pliku jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.Preview.length > 250) {
      const er = new Error('Nazwa wyświetlana jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.Publishers.find(_ => _.Name === publisher.Name)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const id = bag.Publishers.reduce((oa, u) => Math.max(oa, u.Id), 0) + 1
    const now = new Date()
    const created: IPublisher = {
      Id: id,
      Name: publisher.Name,
      Preview: publisher.Preview,
      FileName: publisher.FileName,
      Archived: publisher.Archived || false
    }
    bag.Publishers.push(created)
    bag.Version = 'v' + now.getTime().toString()
    await toLocalStorage(bag)
    return created
  },
  async UpdatePublisher ({}, publisher: IPublisher): Promise<IPublisher> {
    if (!publisher.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!publisher.Preview) {
      const er = new Error('Nazwa wyświetlana jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!publisher.FileName) {
      const er = new Error('Nazwa pliku jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.FileName.length > 150) {
      const er = new Error('Nazwa pliku jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (publisher.Preview.length > 250) {
      const er = new Error('Nazwa wyświetlana jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.Publishers.find(_ => _.Name === publisher.Name && _.Id !== publisher.Id)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const index = bag.Publishers.findIndex(_ => _.Id === publisher.Id)

    if (index >= 0) {
      const now = new Date()
      const old = bag.Publishers[index]
      old.Name = publisher.Name
      old.Preview = publisher.Preview
      old.FileName = publisher.FileName
      old.Archived = publisher.Archived || false
      bag.Version = 'v' + now.getTime().toString()
      await toLocalStorage(bag)
      return old
    } else {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
  },
  async UpdateDecreeType ({}, type: IDecreeType): Promise<IDecreeType> {
    if (!type.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!type.Preview) {
      const er = new Error('Nazwa wyświetlana jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (!type.FileName) {
      const er = new Error('Nazwa pliku jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (type.FileName.length > 150) {
      const er = new Error('Nazwa pliku jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (type.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    if (type.Preview.length > 250) {
      const er = new Error('Nazwa wyświetlana jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.DecreeTypes.find(_ => _.Name === type.Name && _.Id !== type.Id)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const index = bag.DecreeTypes.findIndex(_ => _.Id === type.Id)

    if (index >= 0) {
      const now = new Date()
      const old = bag.DecreeTypes[index]
      old.Name = type.Name
      old.Preview = type.Preview
      old.FileName = type.FileName
      old.Archived = type.Archived || false
      bag.Version = 'v' + now.getTime().toString()
      await toLocalStorage(bag)
      return old
    } else {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
  },
  async UpdateDecreeTag ({}, tag: IDecreeTag): Promise<IDecreeTag> {
    if (!tag.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (tag.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.DecreeTags.find(_ => _.Name === tag.Name && _.Id !== tag.Id)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const index = bag.DecreeTags.findIndex(_ => _.Id === tag.Id)

    if (index >= 0) {
      const now = new Date()
      const old = bag.DecreeTags[index]
      old.Name = tag.Name
      old.Archived = tag.Archived || false
      bag.Version = 'v' + now.getTime().toString()
      await toLocalStorage(bag)
      return old
    } else {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
  },
  async CreateDecreeTag ({}, decreeTag: IDecreeTag): Promise<IDecreeTag> {
    if (!decreeTag.Name) {
      const er = new Error('Nazwa jest wymagana')
      er['statusCode'] = 422
      throw er
    }
    if (decreeTag.Name.length > 150) {
      const er = new Error('Nazwa jest za długa')
      er['statusCode'] = 422
      throw er
    }
    const bag = await fromLocalStorage()
    if (bag.DecreeTags.find(_ => _.Name === decreeTag.Name)) {
      const er = new Error('Nazwa musi być unikatowa')
      er['statusCode'] = 422
      throw er
    }
    const id = bag.DecreeTags.reduce((oa, u) => Math.max(oa, u.Id), 0) + 1
    const now = new Date()
    const created: IDecreeTag = {
      Id: id,
      Name: decreeTag.Name,
      Archived: decreeTag.Archived || false
    }
    bag.DecreeTags.push(created)
    bag.Version = 'v' + now.getTime().toString()
    await toLocalStorage(bag)
    return created
  }
}
