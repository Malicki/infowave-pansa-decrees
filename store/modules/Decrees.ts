import { ActionTree, ActionContext } from 'vuex'
import { RootState } from 'store'
import { IDecree, IDecreeRelationships } from 'model/IDecree'
import { IResult } from '~/model/IResult'
import { IGetDecrees } from '~/model/IGetDecrees'
import { IDictBag } from '~/model/IDictionaryItem'
import { $root } from '../root'
import { IFile } from 'model/IFile'
import Dexie from 'dexie'

export const name = 'Decrees'

const key = '__decrees'
function zeroPad (n: number, length: number = 2): string {
  let str = n.toString() + ''
  while (str.length < length) str = '0' + str
  return str
}

async function fromLocalStorage (delay: number = 150): Promise<Array<IDecree>> {
  if (window['__altDelay']) {
    delay = window['__altDelay'] as number
  }
  return new Promise(function (resolve) {
    const str = window.localStorage.getItem(key) || '[]'
    setTimeout(() => {
      resolve(JSON.parse(str) as Array<IDecree>)
    }, delay)
  })
}

async function toLocalStorage (decrees: Array<IDecree>, delay: number = 150): Promise<void> {
  if (window['__altDelay']) {
    delay = window['__altDelay'] as number
  }
  return new Promise(function (resolve) {
    window.localStorage.setItem(key, JSON.stringify(decrees))
    setTimeout(() => {
      resolve()
    }, delay)
  })
}

function clone<T> (item: T): T {
  return JSON.parse(JSON.stringify(item)) as T
}

function span (n: number): string {
  return n >= 10 ? n.toString() : '0' + n.toString()
}

function getSearchable (obj: Object, props: string[]) {
  return props.map(p => {
    let v = ''
    if (p.indexOf('.') >= 0) {
      v = ((obj[p.split('.')[0]] as Object)[p.split('.')[1]])
    } else {
      v = (obj[p])
    }
    if (typeof v['toISOString'] === 'function') {
      const d = v as any as Date
      v = `${d.getFullYear()}-${ span(d.getMonth() + 1) }-${ span(d.getDate()) }`
      v += `|${ span(d.getDate()) }-${ span(d.getMonth() + 1) }-${d.getFullYear()}`
      v += `|${d.getFullYear()}/${ span(d.getMonth() + 1) }/${ span(d.getDate()) }`
    }
    return v.toLowerCase()
  })
}

export interface State {}

export interface AddMasterFileData {
  File: IFile | null,
  DecreeId: number,
  Public: boolean
}
export interface AttachmentFileData {
  File: IFile,
  DecreeId: number,
  Public: boolean,
  Order: number
}

export interface AlterAttachment {
  Id: string,
  Name: string,
  Public: boolean,
  Order: number,
  Remove: boolean
}

export interface UpdateAttachmentsData {
  DecreeId: number,
  Attachments: Array<AlterAttachment>
}

export interface GetAttachmentFileContentData {
  DecreeId: number,
  AttachmentId: string
}

function ZeroPad (n: number, length: number = 2): string {
  let str = n.toString() + ''
  while (str.length < length) str = '0' + str
  return str
}
function DB () {
  const db = new Dexie('demoFiles') as any
  db.version(1).stores({
    master_files: '++id,blob'
  })
  return db
}

function GenerateMasterFileName (decree: IDecree, data: AddMasterFileData) {
  return `${decree.Type!.FileName}_${decree.Publisher!.FileName}_${(new Date(decree.Date as any)).getFullYear().toString()}`
  + `__${ZeroPad(decree.Uid as number, 3)}.${data.File!.Extension}`
}

export const namespaced = true

export const state = (): State => ({})

export interface Actions<S, R> extends ActionTree<S, R> {
  GetDecreeById (context: ActionContext<S, R>, id: number): Promise<IDecree | null>
  GetDecreeRelationships (context: ActionContext<S, R>, id: number): Promise<IDecreeRelationships>
  CreateDecree (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  ImportDecree (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  UpdateDecree (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  DeleteDecree (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  GetDecrees (context: ActionContext<S, R>, get: IGetDecrees): Promise<IResult<IDecree>>
  LockUid (context: ActionContext<S, R>, id: number): Promise<IDecree | null>
  ReleaseUid (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  PublishDecree (context: ActionContext<S, R>, decree: IDecree): Promise<IDecree>
  AddMasterFile (context: ActionContext<S, R>, data: AddMasterFileData): Promise<IFile | null>
  AddAttachment (context: ActionContext<S, R>, data: AttachmentFileData): Promise<IFile>
  UpdateAttachments (context: ActionContext<S, R>, data: UpdateAttachmentsData): Promise<void>
  GetMasterFileContent (context: ActionContext<S, R>, decreeId: number): Promise<ArrayBuffer>
  GetAttachmentFileContent (context: ActionContext<S, R>, data: GetAttachmentFileContentData): Promise<ArrayBuffer>
}

export const actions: Actions<State, RootState> = {
  async GetAttachmentFileContent (context: ActionContext<State, RootState>, data: GetAttachmentFileContentData): Promise<ArrayBuffer> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const db = DB()
    const decree = await context.dispatch('GetDecreeById', data.DecreeId) as IDecree
    const att = (decree.Attachments || []).find(_ => _.Id === data.AttachmentId)
    if (!att) {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
    const { blob } = await db.master_files.get(parseInt(att.Id as string, 10))
    return blob
  },
  async GetMasterFileContent (context: ActionContext<State, RootState>, decreeId: number): Promise<ArrayBuffer> {
    const db = DB()
    const decree = await context.dispatch('GetDecreeById', decreeId) as IDecree
    const { blob } = await db.master_files.get(parseInt(decree.Master!.Id as string, 10))
    return blob
  },
  async UpdateAttachments (context: ActionContext<State, RootState>, data: UpdateAttachmentsData): Promise<void> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const db = DB()
    const decree = await context.dispatch('GetDecreeById', data.DecreeId) as IDecree
    const now = new Date()
    let event = ''
    let eventId = ''

    const attachments = decree.Attachments || []
    const remove = new Array<number>()
    data.Attachments.forEach(_ => {
      const index = attachments.findIndex(x => x.Id === _.Id)
      const found = index >= 0 ? attachments[index] : null
      if (found) {
        if (_.Remove) {
          attachments.splice(index, 1)
          remove.push(parseInt(found.Id as string, 10))
          event = 'Zaktualizowano załączniki'
          eventId = 'attachments_updated'
        } else {
          found.Name = _.Name
          found.Public = _.Public
          found.Order = _.Order
          event = 'Zaktualizowano załączniki'
          eventId = 'attachments_updated'
        }
      }
    })
    await Promise.all(remove.map(async _ => {
      await db.master_files.where('id').equals(_).delete()
    }))
    decree.Attachments = attachments
    if (eventId) {
      decree.Logs!.push({
        Event: event,
        EventId: eventId,
        Date: now,
        UserId: user.Id.toString(),
        UserName: user.Title
      })
    }
    const all = await fromLocalStorage(0)
    const index = all.findIndex(o => o.Id === decree.Id)
    all[index] = decree
    await toLocalStorage(all)
    return
  },
  async AddAttachment (context: ActionContext<State, RootState>, data: AttachmentFileData): Promise<IFile> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const db = DB()
    const decree = await context.dispatch('GetDecreeById', data.DecreeId) as IDecree
    const now = new Date()
    let event = ''
    let eventId = ''
    const all = await fromLocalStorage(0)
    if (data.File) {
      const id: number = await db.master_files.add({
        blob: data.File.Content
      })
      event = 'Dodano załącznik'
      eventId = 'attachment_created'
      const AttachmentFile = {
        Id: id.toString(),
        Extension: data.File.Extension,
        Type: data.File.Type,
        Name: `${data.File.Name}`,
        OriginName: data.File.OriginName,
        Size: data.File.Size,
        Store: 'IndexedDB',
        Public: data.Public,
        Order: data.Order
      }
      if (eventId) {
        decree.Logs!.push({
          Event: event,
          EventId: eventId,
          Date: now,
          UserId: user.Id.toString(),
          UserName: user.Title,
          Bullets: ['plik: ' + data.File.Name]
        })
      }
      decree.Attachments = decree.Attachments || []
      decree.Attachments.push(AttachmentFile)

      const index = all.findIndex(o => o.Id === decree.Id)
      all[index] = decree
      await toLocalStorage(all)
      return AttachmentFile
    } else {
      const er = new Error('Plik jest wymagany!')
      er['statusCode'] = 400
      throw er
    }
  },
  async AddMasterFile (context: ActionContext<State, RootState>, data: AddMasterFileData): Promise<IFile | null> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const bullets: string[] = []
    const db = DB()
    const decree = await context.dispatch('GetDecreeById', data.DecreeId) as IDecree
    const now = new Date()
    let event = ''
    let eventId = ''
    const all = await fromLocalStorage(0)
    if (data.File === null) {
      if (decree.StatusId === 3) {
        const er = new Error('Opublikowany akt musi posiadać dokument!')
        er['statusCode'] = 422
        throw er
      }
      if (decree.Master) {
        event = 'Usunięto dokument'
        eventId = 'masterfile_removed'
        await db.master_files.where('id').equals(parseInt(decree.Master.Id as string, 10)).delete()
        decree.Master = null
        const index = all.findIndex(o => o.Id === decree.Id)
        all[index] = decree
        if (eventId) {
          decree.Logs!.push({
            Event: event,
            EventId: eventId,
            Date: now,
            UserId: user.Id.toString(),
            UserName: user.Title
          })
        }
        await toLocalStorage(all)
      }
      return null
    } else {
      const id: number = await db.master_files.add({
        blob: data.File.Content
      })
      if (decree.Master) {
        await db.master_files.where('id').equals(parseInt(decree.Master.Id as string, 10)).delete()
        event = 'Zaktualizowano dokument'
        eventId = 'masterfile_updated'
      } else {
        event = 'Dodano dokument'
        eventId = 'masterfile_created',
        bullets.push('plik: ' + GenerateMasterFileName(decree, data))
      }
      const MasterFile = {
        Id: id.toString(),
        Extension: data.File.Extension,
        Type: data.File.Type,
        Name: GenerateMasterFileName(decree, data),
        OriginName: data.File.OriginName,
        Size: data.File.Size,
        Store: 'IndexedDB',
        Public: data.Public
      }
      if (eventId) {
        decree.Logs!.push({
          Event: event,
          EventId: eventId,
          Date: now,
          UserId: user.Id.toString(),
          UserName: user.Title,
          Bullets: bullets
        })
      }
      decree.Master = MasterFile
      const index = all.findIndex(o => o.Id === decree.Id)
      all[index] = decree
      await toLocalStorage(all)
      return MasterFile
    }
  },
  async LockUid (context: ActionContext<State, RootState>, id: number): Promise<IDecree | null> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const decrees = await fromLocalStorage(0)
    const decree = decrees.find(_ => { return _.Id === id }) as IDecree
    const year = (decree.Date as any).split('-')[0]
    if (decree.StatusId !== 1) {
      // throw
      const er = new Error('Nie można nadać numeru aktowi znajdującemu się w tym statusie')
      er['statusCode'] = 422
      throw er
    }
    // collision
    const collidingCount = decrees.filter(_ => {
      return _.Id !== id && _.PublisherId === decree.PublisherId && _.TypeId === decree.TypeId && _.StatusId === 2 && (_.Date as any).split('-')[0] === year
    }).length
    if (collidingCount > 0) {
      // throw
      const er = new Error('Nadawanie numerów dla wybranego typu, wydawcy i roku jest obecnie zablokowane')
      er['statusCode'] = 422
      throw er
    }
    const uids = decrees.filter(_ => {
      return _.Id !== id && _.PublisherId === decree.PublisherId && _.TypeId === decree.TypeId && _.StatusId === 3 && _.Uid !== null && (_.Date as any).split('-')[0] === year
    }).map(_ => {
      return _.Uid as number
    })
    const uid = (uids.length > 0 ? Math.max(...uids) : 0) + 1
    decree.Uid = uid
    decree.StatusId = 2
    const now = new Date()
    decree.Logs!.push({
      Event: 'Nadano numer',
      EventId: 'locked',
      Date: now,
      UserId: user.Id.toString(),
      UserName: user.Title,
      Bullets: ['numer: ' + ZeroPad(uid, 3), 'nowy status: ' + 'Oczekujące na publikację']
    })
    decree.Modified = now
    decree.Version = now.getTime().toString()
    decree.ModifiedById = user.Id

    await toLocalStorage(decrees, 400)
    decree.Date = new Date(decree.Date as any as string)
    decree.Created = new Date(decree.Created as any as string)
    decree.Modified = new Date(decree.Modified as any as string)
    return decree
  },
  async ReleaseUid (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const old = await context.dispatch('GetDecreeById', decree.Id) as IDecree
    if (old.Version !== decree.Version) {
      const er = new Error('Precondition Failed')
      er['statusCode'] = 412
      throw er
    }
    if (old.Master) {
      const er = new Error('Usuń dokument zanim zwolnisz numer!')
      er['statusCode'] = 422
      throw er
    }

    const now = new Date()
    const event = 'Zwolniono nadany numer'
    const eventId = 'uid_released'
    const uid = old.Uid as number
    old.Version = now.getTime().toString(),
    old.Modified = now,
    old.ModifiedById = user.Id,
    old.StatusId = 1,
    old.Uid = null
    old.Logs!.push({
      Event: event,
      EventId: eventId,
      Date: now,
      UserId: user.Id.toString(),
      UserName: user.Title,
      Bullets: ['numer: ' + ZeroPad(uid, 3), 'nowy status: ' + 'Robocze']
    })
    const all = await fromLocalStorage(0)
    const index = all.findIndex(_ => {
      return _.Id === decree.Id
    })
    all[index] = old
    await toLocalStorage(all)
    return old
  },
  async PublishDecree (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const old = await context.dispatch('GetDecreeById', decree.Id) as IDecree
    if (old.Version !== decree.Version) {
      const er = new Error('Precondition Failed')
      er['statusCode'] = 412
      throw er
    }

    if (!old.Master) {
      const er = new Error('Akt musi posiadać dokument')
      er['statusCode'] = 422
      throw er
    }

    const now = new Date()
    const event = 'Opublikowano'
    const eventId = 'published'
    old.Version = now.getTime().toString(),
    old.Modified = now,
    old.ModifiedById = user.Id,
    old.StatusId = 3,
    old.Logs!.push({
      Event: event,
      EventId: eventId,
      Date: now,
      UserId: user.Id.toString(),
      UserName: user.Title,
      Bullets: ['nowy status: ' + 'Opublikowane']
    })
    const all = await fromLocalStorage(0)
    if (old.CanceledIds && old.CanceledIds.length > 0) {
      const toCancel = all.filter(_ => old.CanceledIds.indexOf(_.Id as number) >= 0)
      if (toCancel.length !== old.CanceledIds.length) {
        const er = new Error('Jeden z aktów które chcesz anulować nie istnieje')
        er['statusCode'] = 422
        throw er
      } else if (toCancel.find(_ => _.StatusId !== 3)) {
        const er = new Error('Akt który chcesz anulować nie znajduje się w statusie "Opublikowany"')
        er['statusCode'] = 422
        throw er
      } else if (toCancel.find(_ => !!_.CanceledById)) {
        const er = new Error('Akt który chcesz anulować jest już anulowany')
        er['statusCode'] = 422
        throw er
      }
      toCancel.forEach(_ => {
        _.CanceledById = old.Id as number
        _.Logs = _.Logs || []
        _.Logs.push({
          Event: 'Anulowano',
          EventId: 'canceled',
          Date: now,
          UserId: user.Id.toString(),
          UserName: user.Title
        })
      })
    }
    if (old.ChangedIds && old.ChangedIds.length > 0) {
      const toChange = all.filter(_ => old.ChangedIds.indexOf(_.Id as number) >= 0)
      toChange.forEach(_ => {
        _.ChangedByAny = true
        _.Logs = _.Logs || []
        _.Logs.push({
          Event: 'Zmieniono',
          EventId: 'changed',
          Date: now,
          UserId: user.Id.toString(),
          UserName: user.Title
        })
      })
    }
    const index = all.findIndex(_ => {
      return _.Id === decree.Id
    })
    all[index] = old
    await toLocalStorage(all)
    return old
  },
  async GetDecreeById (context: ActionContext<State, RootState>, id: number): Promise<IDecree | null> {
    const res = await context.dispatch('GetDecrees', { Id: id, Count: false, Desc: false, OrderBy: null, Skip: 0, Top: 1, ExpandDictionaries: true })
    if (res.Value.length === 0) {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
    const decree = res.Value[0]
    if (decree.Deleted === true) {
      const er = new Error('Not Found')
      er['statusCode'] = 404
      throw er
    }
    const user = $root(context.rootState).User.CurrentUser
    const hideNonPublicMasterFiles = (user!.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0)
    if (decree.Master && hideNonPublicMasterFiles) {
      decree.Master = null
    }
    decree.Attachments = decree.Attachments || []
    if (hideNonPublicMasterFiles) {
      decree.Attachments = decree.Attachments.filter(_ => _.Public === true)
    }
    decree.Attachments = decree.Attachments.sort((a, b) => { return a.Order - b.Order })
    return decree
  },
  async GetDecreeRelationships (context: ActionContext<State, RootState>, id: number): Promise<IDecreeRelationships> {
    const allResult = await context.dispatch('GetDecrees', { Count: false, Desc: false, OrderBy: null, Skip: 0, Top: 10000000, ExpandDictionaries: true })

    const all: Array<IDecree> = allResult.Value
    const subject = all.find(_ => _.Id === id) as IDecree
    const user = $root(context.rootState).User.CurrentUser
    const hidePending = (user!.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0)

    const result: IDecreeRelationships = {
      Subject: subject
    }
    const isSubjectPending = subject.StatusId === 1 || subject.StatusId === 2
    if (isSubjectPending) {
      if (subject.CanceledIds) {
        result.PendingCanceled = all.filter(_ => subject.CanceledIds.indexOf(_.Id as number) >= 0)
      }
      if (subject.ChangedIds) {
        result.PendingChanged = all.filter(_ => subject.ChangedIds.indexOf(_.Id as number) >= 0)
      }
    } else {
      if (subject.CanceledIds) {
        result.Canceled = all.filter(_ => subject.CanceledIds.indexOf(_.Id as number) >= 0)
      }
      if (subject.ChangedIds) {
        result.Changed = all.filter(_ => subject.ChangedIds.indexOf(_.Id as number) >= 0)
      }
    }
    if (subject.CanceledById) {
      result.CanceledBy = all.find(_ => _.Id === subject.CanceledById)
    }
    result.ChangedBy = all.filter(_ => _.ChangedIds && _.ChangedIds.indexOf(subject.Id as number) >= 0 && _.StatusId === 3)
    if (!hidePending) {
      result.PendingCanceledBy = all.filter(_ => _.CanceledIds && _.CanceledIds.indexOf(subject.Id as number) >= 0 && _.StatusId !== 3)
      result.PendingChangedBy = all.filter(_ => _.ChangedIds && _.ChangedIds.indexOf(subject.Id as number) >= 0 && _.StatusId !== 3)
    }
    return result
  },
  async GetDecrees (context: ActionContext<State, RootState>, get: IGetDecrees): Promise<IResult<IDecree>> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    let all = await fromLocalStorage()
    if (get.Deleted !== true) {
      all = all.filter(d => d.Deleted !== true)
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      all = all.filter(_ => _.StatusId === 3)
    }

    all.forEach(_ => {
      _.Date = new Date(_.Date as any as string)
      _.Created = new Date(_.Created as any as string)
      _.Modified = new Date(_.Modified as any as string)
      _.ChangedByAny = _.ChangedByAny || false
      _['CanceledByAny'] = !!_.CanceledById
    })
    // expand
    const dict: IDictBag = await context.dispatch('Dictionaries/GetDictionaries', {}, { root: true })
    all.forEach(_ => {
      _.Publisher = clone(dict.Publishers.find(x => x.Id === _.PublisherId))
      _['Publisher.Name'] = _.Publisher!.Name
      _.Type = clone(dict.DecreeTypes.find(x => x.Id === _.TypeId))
      _['Type.Name'] = _.Type!.Name
      _.Tags = clone(dict.DecreeTags.filter(x => _.TagIds.indexOf(x.Id) >= 0))
      _.Status = clone(dict.DecreeStatuses.find(x => x.Id === _.StatusId))
      _['Status.Name'] = _.Status!.Name

      Object.keys(_).forEach(key => {
        const v = _[key]
        if (typeof v === 'string') {
          _['__sort__' + key] = v.toLowerCase()
        } else {
          _['__sort__' + key] = v
        }
      })
    })
    // ToDo: filter
    if ((!get.Id) && get.Filter) {
      const f = get.Filter
      if (f.Case) {
        all = all.filter(_ => {
          return _.Case.toLowerCase().indexOf(f.Case!.toLowerCase()) !== -1
        })
      }
      if (f.Content) {
        all = all.filter(_ => {
          return _.Content.toLowerCase().indexOf(f.Content!.toLowerCase()) !== -1
        })
      }
      if (f.Teams !== null) {
        all = all.filter(_ => {
          return _.Teams === f.Teams
        })
      }
      if (f.Projects !== null) {
        all = all.filter(_ => {
          return _.Projects === f.Projects
        })
      }
      if (f.PublicProcurement !== null) {
        all = all.filter(_ => {
          return _.PublicProcurement === f.PublicProcurement
        })
      }
      if (f.Publisher.length > 0) {
        all = all.filter(_ => {
          return f.Publisher.indexOf(_.PublisherId as number) !== -1
        })
      }
      if (f.Status.length > 0) {
        all = all.filter(_ => {
          return f.Status.indexOf(_.StatusId as number) !== -1
        })
      }
      if (f.Type.length > 0) {
        all = all.filter(_ => {
          return f.Type.indexOf(_.TypeId as number) !== -1
        })
      }
      if (f.Date.length === 2) {
        f.Date[0] = new Date(f.Date[0])
        f.Date[1] = new Date(f.Date[1])
        const start = (new Date(f.Date[0].getFullYear(), f.Date[0].getMonth(), f.Date[0].getDate(), 0, 0, 0)).getTime()
        const end = (new Date(f.Date[1].getFullYear(), f.Date[1].getMonth(), f.Date[1].getDate(), 23, 59, 59)).getTime()
        all = all.filter(_ => _.Date!.getTime() >= start && _.Date!.getTime() <= end)
      }
      if (f.Tags.length > 0) {
        all = all.filter(_ => {
          return f.Tags.filter(t => {
            return _.TagIds.indexOf(t) !== -1
          }).length > 0
        })
      }
      if (f.Uid !== null && f.Uid.length === 2) {
        all = all.filter(_ => {
          return _.Uid !== null && _.Uid >= f.Uid![0] && _.Uid <= f.Uid![1]
        })
      }
    }
    // filter: search
    if (get.Search) {
      let searchArr = get.Search.trim().split(' ').map(_ => { return _.toLowerCase() })
      const searchTags = searchArr.filter(_ => { return _.indexOf('#') === 0 }).map(_ => { return _.replace('#','') })
      searchArr = searchArr.filter(_ => { return _.indexOf('#') !== 0 })
      const searchUids = searchArr.filter(_ => { return _.indexOf('[') === 0 && _.indexOf(']') === 4 })
      .map(_ => { return _.replace('[','').replace(']', '') })
      .map(_ => parseInt(_, 10)).filter(_ => !isNaN(_))
      searchArr = searchArr.filter(_ => { return _.indexOf('[') !== 0 || _.indexOf(']') !== 4 })
      all = all.filter(_ => {
        const searchable = getSearchable(_, ['Case', /*'Content',*/ 'Publisher.Name', 'Type.Name', 'Date', 'Status.Name'])
        if (_.Uid) {
          searchable.push(zeroPad(_.Uid, 3))
        }
        const foundText = searchArr.filter(s => {
          return searchable.find(x => { return x.indexOf(s) >= 0 }) // || (_.Tags!.find(t => { return t.Name.toLowerCase() === s}))
        }).length === searchArr.length
        const foundTags = searchTags.filter(s => {
          return (_.Tags!.find(t => { return t.Name.toLowerCase() === s }))
        }).length === searchTags.length
        return foundTags && foundText
      })
      if (searchUids.length > 0) {
        all = all.filter(_ => (!!_.Uid) && searchUids.indexOf(_.Uid) >= 0)
      }
    }
    // count (ignore if Id provided)
    const count: number | null = (!get.Id) && get.Count ? all.length : null
    // By Id (single or null)
    if (get.Id) {
      const byid = all.find(_ => _.Id === get.Id) || null
      all = byid ? [byid] : []
    } else {
      // sort
      if (get.OrderBy) {
        const av = get.Desc ? -1 : 1
        const p: string = '__sort__' + get.OrderBy
        all = all.sort((a,b) => { return a[p] >= b[p] ? (a[p] > b[p] ? av : 0) : (-av) })
      } else {
        all = all.sort((a,b) => { return (b.Id as number) - (a.Id as number) })
      }
      // pagination
      all = all.slice(get.Skip, get.Top + get.Skip)
    }

    all.forEach(_ => {
      delete _['Publisher.Name']
      delete _['Type.Name']
      delete _['Status.Name']
      Object.keys(_).forEach(key => {
        if (key.indexOf('__sort__') === 0) {
          delete _[key]
        }
      })
    })
    // expand
    if (get.ExpandDictionaries !== true) {
      all.forEach(_ => {
        delete _.Publisher
        delete _.Tags
        delete _.Type
        delete _.Status
      })
    }

    return {
      Value: all,
      Count: count
    }
  },
  async ImportDecree (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const all = await fromLocalStorage()
    let id = all.reduce((oa, u) => Math.max(oa, u.Id as number), 0)
    id += 1
    const now = new Date()
    const created: IDecree = {
      Id: id,
      AuthorId: user.Id,
      ModifiedById: user.Id,
      CanceledIds: [],
      ChangedIds: [],
      Uid: null,
      Version: now.getTime().toString(),
      Created: now,
      Modified: now,
      TypeId: decree.TypeId,
      Case: decree.Case,
      Content: decree.Content,
      Date: decree.Date,
      Projects: decree.Projects,
      PublicProcurement: decree.PublicProcurement,
      Teams: decree.Teams,
      TagIds: decree.TagIds,
      PublisherId: decree.PublisherId,
      StatusId: 10,
      Logs: [{
        Date: now,
        UserId: user.Id.toString(),
        UserName: user.Title,
        EventId: 'import',
        Event: 'Zaimportowano'
      }]
    }
    all.push(created)
    await toLocalStorage(all)
    return created
  },
  async CreateDecree (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const all = await fromLocalStorage()
    if (decree.CanceledIds) {
      const toCancel = all.filter(_ => decree.CanceledIds.indexOf(_.Id as number) >= 0)
      if (toCancel.length !== decree.CanceledIds.length) {
        const er = new Error('Jeden z aktów które chcesz anulować nie istnieje')
        er['statusCode'] = 422
        throw er
      } else if (toCancel.find(_ => _.StatusId !== 3)) {
        const er = new Error('Jeden z aktów które chcesz anulować nie znajduje się w statusie "Opublikowany"')
        er['statusCode'] = 422
        throw er
      } else if (toCancel.find(_ => !!_.CanceledById)) {
        const er = new Error('Jeden z aktów które chcesz anulować jest już anulowany')
        er['statusCode'] = 422
        throw er
      }
    }
    if (decree.ChangedIds) {
      const toChange = all.filter(_ => decree.ChangedIds.indexOf(_.Id as number) >= 0)
      if (toChange.length !== decree.ChangedIds.length) {
        const er = new Error('Jeden z aktów które chcesz zmienić nie istnieje')
        er['statusCode'] = 422
        throw er
      } else if (toChange.find(_ => _.StatusId !== 3)) {
        const er = new Error('Jeden z aktów które chcesz zmienić nie znajduje się w statusie "Opublikowany"')
        er['statusCode'] = 422
        throw er
      } else if (toChange.find(_ => !!_.CanceledById)) {
        const er = new Error('Jeden z aktów które który chcesz zmienić jest już anulowany')
        er['statusCode'] = 422
        throw er
      }
    }
    let id = all.reduce((oa, u) => Math.max(oa, u.Id as number), 0)
    id += 1
    const now = new Date()
    const created: IDecree = {
      Id: id,
      AuthorId: user.Id,
      ModifiedById: user.Id,
      CanceledIds: decree.CanceledIds || [],
      ChangedIds: decree.ChangedIds || [],
      Uid: null,
      Version: now.getTime().toString(),
      Created: now,
      Modified: now,
      TypeId: decree.TypeId,
      Case: decree.Case,
      Content: decree.Content,
      Date: decree.Date,
      Projects: decree.Projects,
      PublicProcurement: decree.PublicProcurement,
      Teams: decree.Teams,
      TagIds: decree.TagIds,
      PublisherId: decree.PublisherId,
      StatusId: 1,
      Logs: [{
        Date: now,
        UserId: user.Id.toString(),
        UserName: user.Title,
        EventId: 'created',
        Event: 'Utworzono wersję roboczą'
      }]
    }
    all.push(created)
    await toLocalStorage(all)
    return created
  },
  async UpdateDecree (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const old = await context.dispatch('GetDecreeById', decree.Id) as IDecree
    if (old.Version !== decree.Version) {
      const er = new Error('Precondition Failed')
      er['statusCode'] = 412
      throw er
    }

    const now = new Date()
    const event = old.StatusId === 1 ? 'Zaktualizowano wersję roboczą' : old.StatusId === 2 ? 'Zaktualizowano Oczekujące na publikację' : 'Zaktualizowano opublikowane'
    const eventId = old.StatusId === 1 ? 'draft_updated' : old.StatusId === 2 ? 'locked_updated' : 'published_updated'
    old.Version = now.getTime().toString(),
    old.Modified = now,
    old.ModifiedById = user.Id,
    old.TypeId = decree.TypeId,
    old.Case = decree.Case,
    old.Content = decree.Content,
    old.Date = decree.Date,
    old.Projects = decree.Projects,
    old.PublicProcurement = decree.PublicProcurement,
    old.Teams = decree.Teams,
    old.TagIds = decree.TagIds,
    old.PublisherId = decree.PublisherId
    old.Logs!.push({
      Event: event,
      EventId: eventId,
      Date: now,
      UserId: user.Id.toString(),
      UserName: user.Title
    })
    const all = await fromLocalStorage(0)
    const index = all.findIndex(_ => {
      return _.Id === decree.Id
    })
    all[index] = old
    await toLocalStorage(all)
    return old
  },
  async DeleteDecree (context: ActionContext<State, RootState>, decree: IDecree): Promise<IDecree> {
    const user = $root(context.rootState).User.CurrentUser
    if (!user) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }

    if (user.Roles.findIndex(_ => _ === 'admin' || _ === 'sekretariat') < 0) {
      const er = new Error('Unauthorized')
      er['statusCode'] = 403
      throw er
    }
    const old = await context.dispatch('GetDecreeById', decree.Id) as IDecree

    if (old.Version !== decree.Version) {
      const er = new Error('Precondition Failed')
      er['statusCode'] = 412
      throw er
    }
    if (old.StatusId === 2) {
      const er = new Error('Zwolnij nadany numer przed usunięciem!')
      er['statusCode'] = 400
      throw er
    }
    if (old.StatusId === 3) {
      const er = new Error('Nie możesz usunąć opublikowanego aktu!\nSkontaktuj się z administratorem.')
      er['statusCode'] = 400
      throw er
    }
    // test only
    // const er = new Error('Forbidden')
    // er['statusCode'] = 403
    // throw er

    const now = new Date()
    const event = 'Usunięto'
    const eventId = 'deleted'
    old.Version = now.getTime().toString(),
    old.Modified = now,
    old.ModifiedById = user.Id,
    old.Deleted = true,
    old.DeletedById = user.Id,
    old.DeletedDate = now,
    old.Logs!.push({
      Event: event,
      EventId: eventId,
      Date: now,
      UserId: user.Id.toString(),
      UserName: user.Title
    })
    const all = await fromLocalStorage(0)
    const index = all.findIndex(_ => {
      return _.Id === decree.Id
    })
    all[index] = old
    await toLocalStorage(all)
    return old
  }
}
