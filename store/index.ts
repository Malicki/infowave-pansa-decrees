import Vuex from 'vuex'
import * as root from './root'
import * as Decrees from './modules/Decrees'
import * as Dictionaries from './modules/Dictionaries'
import * as User from './modules/User'

// More info about store: https://vuex.vuejs.org/en/core-concepts.html
// See https://nuxtjs.org/guide/vuex-store#classic-mode
// structure of the store:
  // types: Types that represent the keys of the mutations to commit
  // state: The information of our app, we can get or update it.
  // getters: Get complex information from state
  // action: Sync or async operations that commit mutations
  // mutations: Modify the state

export type RootState = root.State

const createStore = () => {
  return new Vuex.Store({
    state: root.state(),
    getters: root.getters,
    mutations: root.mutations,
    actions: root.actions,
    modules: {
      [Decrees.name]: Decrees,
      [Dictionaries.name]: Dictionaries,
      [User.name]: User
    }
  })
}

export default createStore
