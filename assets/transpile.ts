import { transpile as ts, ModuleKind, ModuleResolutionKind, ScriptTarget } from 'typescript'

const options = {
  target: ScriptTarget.ESNext,
  module: ModuleKind.ESNext,
  moduleResolution: ModuleResolutionKind.Classic,
  lib: ['esnext', 'esnext.asynciterable'],
  esModuleInterop: true,
  experimentalDecorators: true,
  allowJs: true,
  sourceMap: false,
  strict: true,
  allowSyntheticDefaultImports: true,
  noImplicitAny: false,
  noEmit: true,
  noUnusedLocals: true,
  noUnusedParameters: true,
  removeComments: true,
  diagnostics: true,
  extendedDiagnostics: true
}

export function transpile (code: string): string {
  return ts(code, options)
}
