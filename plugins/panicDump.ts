if (!process.server) {
  const key = '--panic-dump--'
  const now = (new Date()).getTime()
  const current = JSON.parse(window.localStorage.getItem(key) || '{}')
  window.localStorage.removeItem(key)
  if (now - current['time'] <= 10000) {
    Object.keys(current).forEach(k => {
      if (k.indexOf('--dump--') === 0) {
        window.sessionStorage.setItem(k, current[k])
      }
    })
  }

  window.addEventListener('beforeunload', () => {
    const dump = {
      time: (new Date()).getTime()
    }
    Object.keys(window.sessionStorage).forEach(k => {
      if (k.indexOf('--dump--') === 0) {
        dump[k] = window.sessionStorage.getItem(k)
      }
    })
    if (typeof window['--dump-current--'] === 'function') {
      const panic = window['--dump-current--']()
      if (panic && panic.key) {
        dump[panic.key] = panic.value
      }
    }
    window.localStorage.setItem(key, JSON.stringify(dump))
  }, false)
}
