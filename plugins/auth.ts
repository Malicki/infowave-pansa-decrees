import { $root } from '~/store/root'

const anonymous = ['index', 'signin', 'adfs', '404', 'not_found', '403', '500']
const sekretariat = ['decrees-create', 'decrees-edit', 'decrees-files', 'generator', 'dictionaries']
const admin = ['healthcheck']

export default async function ({ app, store }) {
  if (!process.server) {
    const lastCode = window.localStorage.getItem('__lastCode')
    if (lastCode) {
      await store.dispatch('User/SignInWithCode', lastCode)
    }
    app.router.onReady(() => {
      if (!$root(store.state).User.Authorized) {
        if (anonymous.indexOf(app.router.currentRoute.name) < 0) {
          store.$router.push('/signin?next=' + encodeURIComponent(app.router.currentRoute.fullPath))
        }
      } else {
        const roles = $root(store.state).User.CurrentUser!.Roles
        const route = app.router.currentRoute
        if (admin.indexOf(route.name) >= 0) {
          if (roles.indexOf('admin') < 0) {
            store.$router.push('/403')
          }
        } else if (sekretariat.indexOf(route.name) >= 0) {
          if (roles.indexOf('admin') < 0 && roles.indexOf('sekretariat') < 0) {
            store.$router.push('/403')
          }
        }
      }
    })
  }
}
