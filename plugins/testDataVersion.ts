if (!process.server) {
  const key = '__decress_version'
  const version = '0.5.2'
  const ls = window.localStorage
  if ((ls.getItem('__decrees') !== null) || ls.getItem('__dictionaries') !== null) {
    if (ls.getItem(key) !== version) {
      ls.removeItem('__decrees')
      ls.removeItem('__dictionaries')
      ls.setItem(key, version)
      indexedDB.deleteDatabase('demoFiles')
      window.alert('Nowa wersja systemu została załadowana!\nUsuwam niekompatybilne dane testowe')
    }
  } else {
    ls.setItem(key, version)
  }
}
